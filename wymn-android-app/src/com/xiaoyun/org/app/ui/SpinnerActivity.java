package com.xiaoyun.org.app.ui;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.xiaoyun.org.R;

public class SpinnerActivity extends Activity {

	private   String[] array = null; 

	private Spinner spinner;
	private ArrayAdapter<String> adapter;
	 private SharedPreferences preferences = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		 array = this.getResources().getStringArray(R.array.hights); 
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_spinner);
		spinner = (Spinner) findViewById(R.id.Spinner01);
		// ����ѡ������ArrayAdapterl����4
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, array);
		// ������-�б�ķ��
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// ��adapter ��ӵ�spinner��
		spinner.setAdapter(adapter);
		// ����¼�Spinner�¼�����
		spinner.setOnItemSelectedListener(new SpinnerSelectedListener());
		// ����Ĭ��ֵ
		preferences = getSharedPreferences("imgH", Context.MODE_PRIVATE);
		  // remdname
	        String img_hight=preferences.getString("img_hight", "");
	        int n = 0;
	        for (int i = 0; i < array.length; i++) {
	        	if (img_hight.equals(array[i])) {
	        		n = i;
				}
			}
		spinner.setSelection(n,true);
	}

	// ʹ��������ʽ����
	class SpinnerSelectedListener implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			preferences=getSharedPreferences("imgH", Context.MODE_PRIVATE);
		     SharedPreferences.Editor edit=preferences.edit();
		     edit.putString("img_hight", array[arg2]);
		     edit.commit();

			// view.setText("���Ѫ���ǣ�"+m[arg2]);
		}

		public void onNothingSelected(AdapterView<?> arg0) {
		}
	}
}
